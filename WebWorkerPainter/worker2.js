var initialColor = 10;
isFirstTime = true;

onmessage = function (e) {    
    var imageArray = e.data.data;
    for (var index = 0; index < imageArray.length; index += 4) {
        imageArray[index] = initialColor;
        imageArray[index + 1] = initialColor;
        imageArray[index + 2] = initialColor;
        imageArray[index + 3] = 255;
    }
    initialColor--;
    initialColor = initialColor == -1 ? 255 : initialColor;
    postMessage(e.data);
}
